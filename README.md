# Clap For Freedom

## The Project

This project allows you to turn a Raspberry Pi into a Trump quote machine. Upon detecting a clap pattern (default 3 claps), a random Trump quote is picked and played.
We have picked out 23 of the most devastating Trump quotes that even the most staunch of conservatives can't get behind. In fact, many of them are likely to offend conservatives more than lefties: as Trump undermines war heros, fakes religious observance, destroys the sanctity of marriage, etc.

Elevate free speech: let Trump speak for himself and be heard.

## Background

The basic assumption and premise of this project is that Donald J Trump, the 45th president of the USA, is a scourge on the world and needs not to be elected for a second term.

Casting aside the usual partisan arguments, Trump is the most dishonest president in living history, spreads and feeds on division, and employs a fascistic authoritarian mindset.
We have private ICE facilities doing medical experiments on women. We have support for militias in the streets. We have wide-spread denial of what is happening in reality, including science.
We also have growing violence from part of the far left, under a president who would rather tear people apart than lead them back together as one nation.

This is clearly a political project, with arguable bias, but I don't think any of the above is unclear.

In the authors opinion, trying to convert conservatives to lefties by virtual signaling inclusivity, or espousing how disagreeable you find conservative or capitalist thought, is at best pointless and at worst counter-productive (they'll just mock you).
For every argument about income inequality you have, they have an argument about incentive structures and people keeping what they earn. For every argument about treating people fairly you have, they have an argument about how adversity maketh the man and is necessary for freedom. It's difficult, often impossible, to get past people's core philosophies, temperament, and upbringing.

To best defeat Trump, you need to:
i) Get out the vote from the leftie base (who are notorious for not turning out);
ii) Persuade moderates, who usually hate extremism;
iii) Make sure conservative voters are pulled out of their bubble and have to see who Trump really is - so they either skip this one out, throw out their vote on some obscure candidate, or vote Democratic.

This project focuses on 'iii'. News networks such as Fox News, or independent conservative outlets, will do whatever they can to hide Trump's failings, build up controversies on the left, and push narratives that push people to their own desired political outcomes.
It is our job to make it very clear what Trump really is.

What did you do to try and stop the spread of fascism?

## Hardware

### Computer

This project has been tested on a Raspberry Pi Model B (i.e. a pretty old model). It should work with any Raspberry Pi, or other similar Linux-based devices.

The original developer actually regrets not using one with built in wifi, and ended up getting a USB wifi adapter. This is one that works with the Raspberry Pi: https://www.amazon.com/LOTEKOO-150Mbps-Adapter-Wireless-Raspberry/dp/B06Y2HKT75. The original developer actually used another one that was more effort to set up so we won't recommend that one.

### Sound output

You can create a sound card using a USB dongle (e.g. https://www.amazon.com/gp/product/B081DHGBST) if your device doesn't have one built in. The Raspberry Pi Model B does have one built-in though, so no need for me.

You can use a small cheap speaker. The original developer used https://www.ebay.com/itm/3-5mm-Plug-Mini-Portable-Outdoor-USB-Interface-HD-Phone-Speaker-lamb/402329186024. This model is battery operated. To avoid embarrassment if batteries run out, you may want to plug it in with virtual batteries, e.g. https://www.amazon.com/dp/B08746NMV4/ref=sspa_dk_detail_2 - and then use a power extension cable back to your outlet.

You may want a nice long extension cable (e.g. https://www.amazon.com/Headphone-Extension-Ancable-Earphone-Smartphone/dp/B0882YZCWG).

### Sound input

For a microphone you need something that it is sensitive enough to work from a distance. The original developer used https://www.amazon.com/gp/product/B06XCKGLTP because he already had it. Something cheaper would probably be fine.

The Raspberry Pi is a simple low power device, the USB may be a bit glitchy if you use one. The project goes to extra effort to reconnect to the USB mic if it glitches out.

To extend the mic further away from your computer, you may want a USB repeater cable (necessary for long lengths). The original developer used https://www.amazon.com/gp/product/B0052EXWOK.

### Yard sign

We suggest making some kind of yard sign of your own, and taping the mic and speaker to the back of it, with tape over anything you don't want exposed to the elements. Transparent packing tape is easy to get and doesn't block the speaker/mic too much.

Perhaps say something like "Clap repeatedly for free speech" with American iconography (bald eagles, the liberty torch, etc).

You can get a laminator pretty cheaply, or make something out of wood.

## Software

This project has been tested on Ubuntu, and Raspbian.

Dependencies:
```
sudo apt-get install libasound2-dev build-essential git alsa-utils screen
```

## Usage

git clone this project:
```
git clone https://gitlab.com/occhris/clap-for-freedom.git
```

Then build it:
```
chmod +x *.sh
./build.sh
```

Then launch Clap For Freedom:
```
program/clapforfreedom
```

If the "default" audio devices are not correct/working (they were not on my Raspberry Pi for some reason), you can choose devices using `-i` and `-o` parameters with device names, which you can find from:
```
arecord -l
aplay -l
```
It will tell you card numbers and subdevice numbers, which are translated to like `hw:<card>,<subdevice>`.

For all parameters:
```
program/clapforfreedom -h
```

## Usage via `screen`

Launch Clap For Freedom:
```
./init.sh
```

(parameters sent to `init.sh` will be relayed to the program)

It will launch using `screen`, meaning it can run in the background. Very nice if you are managing your system remotely via SSH. Do "ctrl+a D" to detach if you want to leave it running and exit your shell.

You can resume it with:
```
./resume.sh
```

You can always check the log with:
```
tail screenlog.0
```

## Implementation details

A clap is defined as a sound:
 - across a minimum number of sub-bands (frequencies) at once (a clap is inherently noisy, distinguished from purer sounds);
 - where the amplitude on each of these sub-band a sensitivity threshold;
 - sustained over 5 measurement cycles (so things are averaged out a bit)

We recommend detecting a series of claps to prevent false positives.

## Contributions

Pull requests are welcomed. Especially ones that improve the instructions to make this easier for folks.

If you want to fork this project and replace the quotes to show how sleepy Joe Biden is, or how conservative he actually is, I welcome this too. I would be very surprised if you can find the same volume of material, but more speech is good. Let's give people more facts on which to vote on, rather than the partisan propaganda modern news networks push.

## Libraries

The workhorse behind Clap For Freedom is libclap, a Linux clap detection library written in C using FFT transformations.

**libclap Author:** Lukasz Czyz  
**E-mail:** lukasz.czyzz@gmail.com

This lipclap author has no involvement or knowledge of this project, at the time of writing at least.

Wave files are played by calling aplay from alsa-utils. It needs to be installed and in the path.
Wave files are simple and fast - no delay, no dependencies.
