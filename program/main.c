#include <stdio.h>
#include <stdarg.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>

#include <clap.h>

#include "loc_time.h"

static void output_message(const char *msg, ...);
static void output_error(const char *msg, ...);


/* Default parameters */
#define DEFAULT_SENSITIVITY			100
#define DEFAULT_SUBBANDS_FOR_CLAP	10
#define DEFAULT_ACTION_CLAP_CNT		3
#define DEFAULT_MIN_CLAP_INTV		200
#define DEFAULT_MAX_CLAP_INTV		2000

#define LIMIT_AROUND_PLAYBACK		200


/* Parameters */
char *s_card_speaker;
char *s_card_mic;
int sensitivity;
int subbands_for_clap;
int action_clap_cnt;
int min_clap_intv;
int max_clap_intv;
int debugging;
int muted;

/* variables used in signal handler, must be volatile */ 
volatile int exiting = 0;

int num_audio_files;
char audio_files[50][50]; /* Up to 50 audio files of 50 byte filenames max */

struct clap_cfg *ccfg = NULL;


static void output_message(const char *msg, ...)
{
	char time_str[20];
	struct tm *timestamp;
	time_t now = time(0);
	timestamp = localtime(&now);
	strftime(time_str, sizeof(time_str), "%Y-%m-%d %H:%M:%S", timestamp);

	char fmt[256];
	va_list arglist;

	va_start(arglist, msg);

	sprintf(fmt, "%s %s\n", time_str, msg);
	vfprintf(stdout, fmt, arglist);

	va_end(arglist);
}

static void output_error(const char *msg, ...)
{
	char time_str[20];
	struct tm *timestamp;
	time_t now = time(0);
	timestamp = localtime(&now);
	strftime(time_str, sizeof(time_str), "%Y-%m-%d %H:%M:%S", timestamp);

	char fmt[256];
	va_list arglist;

	va_start(arglist, msg);

	sprintf(fmt, "%s %s\n", time_str, msg);
	vfprintf(stderr, fmt, arglist);

	va_end(arglist);
}

void play_random_audio()
{
	int audio_file_index;
	char *audio_file;

	char *cmd;

	audio_file_index = rand() % num_audio_files;
	audio_file = audio_files[audio_file_index];

	cmd = malloc(30 + strlen(audio_file) + strlen(s_card_speaker));
	strcpy(cmd, "aplay --device=");
	strcat(cmd, s_card_speaker);
	strcat(cmd, " ");
	strcat(cmd, "audio/");
	strcat(cmd, audio_file);

	if (muted) {
		output_message("Would be calling if not muted; %s", cmd);
	} else {
		output_message("Calling %s", cmd);

		if (system(cmd) != 0) {
			output_message("Error calling aplay");

			clap_free(ccfg);
			exit(0);
		}
	}
}

void process_clap()
{
	static int clap_cnt = 0;
	static msec_t last_play_time = 0;
	static msec_t last_clap_time = 0;
	static int locked = 0;

	msec_t now = get_program_time();
	msec_t intv_since_last_play = now - last_play_time;
	msec_t intv_since_last_clap = now - last_clap_time;

	if (locked == 1) {
		return; /* We do not want claps detected when playing audio already, esp as audio could generate false positives */
	}

	if ((last_play_time != 0) && (intv_since_last_play < LIMIT_AROUND_PLAYBACK)) {
		clap_cnt = 0;

		output_message("CLAPS, BUT DETECTED AROUND PLAYBACK, IGNORED; now @ %d", clap_cnt);
	} else if  ((last_clap_time != 0) && (intv_since_last_clap < min_clap_intv)) {
		output_message("CLAP, BUT TOO SOON SINCE PREVIOUS, IGNORED; now @ %d", clap_cnt);
	} else if ((last_clap_time != 0) && (action_clap_cnt > 1) && (intv_since_last_clap > max_clap_intv)) {
		clap_cnt = 1;

		output_message("CLAP, BUT TOO LONG SINCE PREVIOUS, RESETTING; now @ %d", clap_cnt);
	} else {
		clap_cnt++;

		if (clap_cnt >= action_clap_cnt) {
			output_message("CLAP, ACTIONING @ %d", clap_cnt);

			locked = 1;
			play_random_audio();
			locked = 0;

			last_play_time = get_program_time();
			clap_cnt = 0;
			output_message("Finished playback");
		} else {
			output_message("CLAP, STILL COUNTING UNTIL %d; now @ %d", action_clap_cnt, clap_cnt);
		}
	}

	last_clap_time = get_program_time();
}

int scan_audio_files()
{
	DIR *dir;
	struct dirent *ent;
	char *point;

	num_audio_files = 0;

	if ((dir = opendir ("audio")) == NULL) {
		/* could not open directory */
		return 0;
	}

	/* print all the files and directories within directory */
	while ((ent = readdir (dir)) != NULL) {
		if ((point = strrchr(ent->d_name, '.')) != NULL) {
			if (strcmp(point, ".wav") == 0) {
				strcpy(audio_files[num_audio_files], ent->d_name);
				num_audio_files++;
				output_message("Found audio file %s", ent->d_name);
			}
		}
	}
	closedir (dir);

	return 1;
}

void print_usage()
{
	fprintf(stderr,
		"Usage:\n"
		"clapforfreedom [parameters]\n"
		"\n"
		"Parameters:\n"
		"Set microphone device... -i <mic_device=\"default\">\n"
		"Set speaker device... -o <speaker_device=\"default\">\n"
		"Set sensitivity... -s <sensitivity=%i>\t(1 or higher; amplitude threshold for detecting a frequency on a subband)\n"
		"Set subbands required... -c <subbands-for-clap=%i>\t(how many of the 32 frequency subbands need to be hit for a clap to be detected)\n"
		"Set number of claps required... -n <number-claps=%i>\n"
		"Set minimum time between claps... -a <minimum-time-between-claps=%i>\t(in milliseconds)\n"
		"Set maximum time between claps... -b <maximum-time-between-claps=%i>\t(in milliseconds)\n"
		"Enable verbose debug messages about clap detection... -D\n"
		"Do not actually play audio (mute)... -m\n",
		DEFAULT_SENSITIVITY,
		DEFAULT_SUBBANDS_FOR_CLAP,
		DEFAULT_ACTION_CLAP_CNT,
		DEFAULT_MIN_CLAP_INTV,
		DEFAULT_MAX_CLAP_INTV
	);
}

int init_libclap()
{
	ccfg = clap_init(s_card_mic, process_clap, sensitivity, subbands_for_clap, debugging);
	if (ccfg == NULL) {
		output_error("could not init clap module");
		return 0;
	}
	return 1;
}

void exit_sig_handler(int sig)
{
	exiting = 1;
}

void reset_sig_handler(int sig)
{
	while (1) {
		output_error("waiting a little to reinitialize libclap");
		sleep(10); /* Wait for plug and play to reinitialize */
		if (init_libclap() == 1) break;
		output_error("trying to reinitialize libclap");
	}
}

int main(int argc, char *argv[])
{	
	struct sigaction sig_exit_action, sig_reset_action;
	sigset_t block_mask, old_mask, empty_mask;
	int option;

	/* start from defaults */
	s_card_mic = "default";
	s_card_speaker = "default";
	sensitivity = DEFAULT_SENSITIVITY;
	subbands_for_clap = DEFAULT_SUBBANDS_FOR_CLAP;
	action_clap_cnt = DEFAULT_ACTION_CLAP_CNT;
	min_clap_intv = DEFAULT_MIN_CLAP_INTV;
	max_clap_intv = DEFAULT_MAX_CLAP_INTV;
	debugging = 0;
	muted = 0;

	/* parsing command line options */
	while ((option = getopt(argc, argv,"i:o:s:c:n:a:b:hDm")) != -1) {
		switch (option) {
		case 'i':
			s_card_mic = optarg;
			break;
		case 'o':
			s_card_speaker = optarg;
			break;
		case 's':
			sensitivity = atoi(optarg);
			break;
		case 'c':
			subbands_for_clap = atoi(optarg);
			break;
		case 'n':
			action_clap_cnt = atoi(optarg);
			break;
		case 'a':
			min_clap_intv = atoi(optarg);
			break;
		case 'b':
			max_clap_intv = atoi(optarg);
			break;
		case 'D':
			debugging = 1;
			break;
		case 'm':
			muted = 1;
			break;
		case 'h':
		default:
			print_usage();
			goto exit;
		}
	}

	if (!scan_audio_files() || num_audio_files == 0) {
		output_error("Could not read 'audio' directory");
		goto exit;
	}

	/* SIGINT and SIGTERM will cause application to exit */
	memset(&sig_exit_action, 0, sizeof(sig_exit_action));
	sig_exit_action.sa_handler = exit_sig_handler;
	if (sigaction(SIGINT, &sig_exit_action, NULL) != 0) {
		output_error("could not set signal handler");
		goto exit;
	}
	if (sigaction(SIGTERM, &sig_exit_action, NULL) != 0) {
		output_error("could not set signal handler");
		goto exit;
	}

	/* SIGUSR1 is from libclap to reset itself due to losing the mic */
	memset(&sig_reset_action, 0, sizeof(sig_reset_action));
	sig_reset_action.sa_handler = reset_sig_handler;
	if (sigaction(SIGUSR1, &sig_reset_action, NULL) != 0) {
		output_error("could not set signal handler");
		goto exit;
	}

	/* exit signals must be blocked to prevent race-conditions (we handle them manually anyway) */
	sigemptyset(&empty_mask);
	sigemptyset(&block_mask);
	sigaddset(&block_mask, SIGTERM);
	sigaddset(&block_mask, SIGINT);
	if (sigprocmask(SIG_BLOCK, &block_mask, &old_mask) < 0) {
		output_error("could not set signal mask");
		goto exit;
	}

	init_time();

	srand(time(NULL));

	if (!init_libclap()) goto exit;

	while (!exiting) {
		output_message("Suspending main thread");
		sigsuspend(&empty_mask);
		output_message("Woke up main thread");
		if (!exiting) output_message("Looping");
	}

	output_message("terminating...");

exit:
	clap_free(ccfg);
	exit(0);
}
